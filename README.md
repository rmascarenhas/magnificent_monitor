# Magnificent Health Check

This service monitors the health of the "magnificent" service. It has two
components:

* A background routine that periodically issues requests to magnificent and
  verifies whether they are successful or not.
* An HTTP endpoint (`/stats`), that provides simple health statistics in JSON
  format.

### Running

The command line interface provides serveral parameters. Running without
arguments provides a description of the expected values:

```console
% go run monitor.go
Usage: go run monitor.go [magnificent-address] [api-address] [check-frequency] [log-capacity]
        magnificent-address: where the magnificent service is running
        api-address: address used to provide statistics of the magnificent service
        check-frequency: how often to ping magnificent (in seconds)
        log-capacity: maximum number of health checks to keep in memory
```

##### Example

```console
% go run monitor.go localhost:4444 http://localhost:12345 1 100
2019/06/08 11:50:07 Starting monitor service at http://localhost:4444 (magnificent: http://localhost:12345)
```

The command above starts a health monitor that:

* Listens on address `localhost:4444`. Statistics can be viewed at
  `localhost:4444/stats`;
* Monitors the magnificent service that is expected to be running at
  `http://localhost:12345`;
* Issues a request to the magnificent service every second;
* Keeps the most 100 requests in memory; statistics are related to the entries.

An easy way to monitor the collected statistics is with the `watch` command (we
use Python to pretty-print the JSON response):

```console
% watch -n3 'curl -s localhost:4444/stats | python -m json.tool'
Every 3.0s: curl -s localhost:4444/stats | python -m json.tool

{
    "connection_errors": 0,
    "http_status": {
        "200": 5,
        "500": 1
    },
    "log_size": 6,
    "up": true
}
```

### Assumptions

* HTTP status code is sufficient to determine whether the service is up or not.
  Faulty services could return invalid or unusable responses while still
  returning a 200 status.
* Service is deemed to be "up" if the ratio of success responses is above a
  certain threshold (by default, 70%). Choosing the frequency in which the
  checks are made, and the size of the log kept in memory is an
  application-specific tradeoff the user has to make in order for the `up` field
  to be more accurate.
