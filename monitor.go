package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

const (
	// service is considered "up" if at least 70% of latest requests succeeded
	SUCCESS_THRESHOLD = 0.7
)

func usage() {
	fmt.Fprintf(os.Stderr, "Usage: go run monitor.go [magnificent-address] [api-address] [check-frequency] [log-capacity]\n")
	fmt.Fprintf(os.Stderr, "\tmagnificent-address: where the magnificent service is running\n")
	fmt.Fprintf(os.Stderr, "\tapi-address: address used to provide statistics of the magnificent service\n")
	fmt.Fprintf(os.Stderr, "\tcheck-frequency: how often to ping magnificent (in seconds)\n")
	fmt.Fprintf(os.Stderr, "\tlog-capacity: maximum number of health checks to keep in memory\n")

	os.Exit(1)
}

// MagnificentResponse corresponds to an HTTP response obtained from
// Magnificent after a periodic health check.
type MagnificentResponse struct {
	connectionError bool   // whether we failed to even get a response from Magnificent
	status          int    // HTTP status of the response obtained from Magnificent
	body            []byte // raw response body, for analysis
}

// RequestHistory keeps track of previous requests made to the
// magnificent service.
type RequestHistory struct {
	data     []*MagnificentResponse // responses collected over time
	capacity int                    // maximum number of entries to keep in the log
	lock     sync.Mutex             // protects concurrent access to the history
}

func NewRequestHistory(capacity int) *RequestHistory {
	return &RequestHistory{
		data:     []*MagnificentResponse{},
		capacity: capacity,
	}
}

func (history *RequestHistory) add(response *MagnificentResponse) {
	history.lock.Lock()
	defer history.lock.Unlock()

	// do not keep more entries than specified capacity
	if len(history.data) >= history.capacity {
		history.data = history.data[1:]
	}

	history.data = append(history.data, response)
}

// ConnectionError indicates that an attempt to reach the magnificent
// service failed and no response was obtained (i.e., the process
// panicked, there was a network error/timeout, etc).
func (history *RequestHistory) ConnectionError() {
	history.add(&MagnificentResponse{
		connectionError: true,
	})
}

// AddResponse inserts a new HTTP response obtained from magnificent
// into our history.
func (history *RequestHistory) AddResponse(status int, body []byte) {
	history.add(&MagnificentResponse{
		connectionError: false,
		status:          status,
		body:            body,
	})
}

func (history *RequestHistory) Stats() *MagnificentStats {
	// do not allow log to be modified while we collect statistics.
	history.lock.Lock()
	defer history.lock.Unlock()

	total := len(history.data)
	connectionErrors := 0
	httpStatus := map[int]int{}

	for _, response := range history.data {
		if response.connectionError {
			connectionErrors++
		} else {
			httpStatus[response.status]++
		}
	}

	// we determine if the service is up if the number of successful
	// calls to the magnificent service is at least our expected
	// success rate
	up := (float64(httpStatus[http.StatusOK]))/float64(total) > SUCCESS_THRESHOLD

	return &MagnificentStats{
		Up:               up,
		LogSize:          total,
		ConnectionErrors: connectionErrors,
		Status:           httpStatus,
	}
}

// MagnificentStats groups statistics that can be checked by the user
// via the endpoint provided by this monitor service.
//
// LogSize == ConnectionErrors + Sum(every value in the Status map)
type MagnificentStats struct {
	Up               bool        `json:"up"`                // whether the system is considered to be "up"
	ConnectionErrors int         `json:"connection_errors"` // how many connection errors were observed
	LogSize          int         `json:"log_size"`          // total number of entries in the log
	Status           map[int]int `json:"http_status"`       // observed number of responses for each HTTP status
}

// Monitor encapsulates the service that checks the health of the
// magnificent service. It can expose an endpoint that provides
// statistics on previous requests made, at configurable frequencies.
type Monitor struct {
	address            string          // address for the /stats endpoint
	magnificentAddress string          // address of the magnificent service
	frequency          int             // frequency of the checks (in seconds)
	history            *RequestHistory // all known requests made to magnificent
}

func NewMonitor(address, magnificentAddress string, frequency, capacity int) *Monitor {
	return &Monitor{
		address:            address,
		magnificentAddress: magnificentAddress,
		frequency:          frequency,
		history:            NewRequestHistory(capacity),
	}
}

// performChecks calls the magnificent service on the configured
// frequency and collects success/failure statistics.
func (monitor *Monitor) performChecks() {
	ticker := time.NewTicker(time.Duration(monitor.frequency) * time.Second)

	for {
		<-ticker.C

		response, err := http.Get(monitor.magnificentAddress)

		// if we failed to reach the magnificent service for some
		// reason, register the connection error and try again on the
		// next clock tick.
		if err != nil {
			monitor.history.ConnectionError()
			continue
		}

		body, err := ioutil.ReadAll(response.Body)

		// if, for some reason, we fail to read the response body,
		// ignore this request and try again on the next clock tick
		if err != nil {
			log.Printf("Warning: could not read response body, ignoring this request: %v", err)
			continue
		}

		monitor.history.AddResponse(response.StatusCode, body)
	}
}

// CollectStats is an endpoint provided by this monitor service that
// displays statistics from previous requests made to the Magnificent
// service. See definition of the MagnificentStats struct for
// information on data returned by this endpoint.
func (monitor *Monitor) CollectStats(w http.ResponseWriter, r *http.Request) {
	stats := monitor.history.Stats()
	json.NewEncoder(w).Encode(stats)
}

// ListenAndServe sets up handlers for the /stats endpoint and starts
// an HTTP server at the configured address. It also sets up periodic
// checks on the magnificent service. Blocks indefinitely.
func (monitor *Monitor) ListenAndServe() {
	log.Printf("Starting monitor service at http://%s (magnificent: %s)", monitor.address, monitor.magnificentAddress)

	mux := http.NewServeMux()
	mux.HandleFunc("/stats", monitor.CollectStats)

	server := &http.Server{
		Addr:    monitor.address,
		Handler: mux,
	}

	go monitor.performChecks()

	if err := server.ListenAndServe(); err != nil {
		log.Fatalf("Could not start proxy server: %v", err)
	}
}

// parseNatural receives a command line argument and tries to parse it
// as a natural number (i.e., an integer > 0). Process terminates if
// argument is invalid.
func parseNatural(name, value string) int {
	nat, err := strconv.Atoi(value)
	if err != nil || nat <= 0 {
		log.Fatalf("Invalid %s: %v", name, value)
	}

	return nat
}

func main() {
	if len(os.Args) != 5 {
		usage()
	}

	// parse command line arguments
	magnificentAddress := os.Args[1]
	apiAddress := os.Args[2]
	frequency := parseNatural("frequency", os.Args[3])
	capacity := parseNatural("capacity", os.Args[4])

	monitor := NewMonitor(magnificentAddress, apiAddress, frequency, capacity)

	// runs indefinitely
	monitor.ListenAndServe()
}
